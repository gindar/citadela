from webserv import RequestHandler, WebMaker
from Citadela.helpers import GlobObject, IEvents


class CitRequestHandler(RequestHandler, GlobObject, IEvents):
    def dispatcher(self, path):
        if path == "/results":
            self.pageResults(path)
        elif path == "/results/detail":
            self.pageDetailResults(path)
        elif path == "/" or path == "/status":
            self.pageStatus(path)
        elif path == "/play":
            self.event("playGames")
            self.pageStatus(path)
        else:
            self.page404(path)

    def pageResults(self, path):
        server_status = self.getGlobal("server_status")
        strategies = self.getGlobal("strategies")
        order = self.getGlobal("order")
        results = self.getGlobal("results")
        wm = WebMaker("main.tpl")
        wm.title = "Results"
        if server_status == 3:
            thead = ["#", "Strategy", "Wins"]
            restable = []
            prevwins = 9999
            n = 0
            index = 0
            fs = 16
            for strat in order:
                res = results[strat]
                strategy = strategies[strat]
                if index == 0:
                    fs = 32
                elif index == 1:
                    fs = 28
                elif index == 2:
                    fs = 24
                else:
                    fs = 18

                wm.addContent("<b style='font-size: %dpx;'>#%d %s by %s [%d]</b><br/>" % (fs, index + 1, strategy.name, strategy.author, res[0]))
                index += 1
        else:
            wm.addContent("No results")
        self.flushPage(wm)

    def pageDetailResults(self, path):
        server_status = self.getGlobal("server_status")
        strategies = self.getGlobal("strategies")
        order = self.getGlobal("order")
        results = self.getGlobal("results")
        wm = WebMaker("main.tpl")
        wm.title = "Results"
        if server_status == 3:
            thead = ["#", "Strategy", "Version", "Author", "Wins", "battlesWon"]
            restable = []
            prevwins = 9999
            n = 0
            for strat in order:
                res = results[strat]
                if res[0] != prevwins:
                    n += 1
                prevwins = res[0]
                strategy = strategies[strat]
                restable.append([n, strategy.name, strategy.version, strategy.author, res[0], res[1]])
            wm.addTable(restable, thead, "width='99%'")
        else:
            wm.addContent("No results")
        self.flushPage(wm)

    def pageStatus(self, path):
        server_status = self.getGlobal("server_status")
        wm = WebMaker("main.tpl")
        wm.title = "Status"
        wm.addContent("Welcome to citadela.")

        wm.addContent("<br/>Server port: <b>%s</b>" % self.getGlobal("server_port"))
        wm.addContent("<br/>")
        if server_status == 1:
            strategies = self.getGlobal("strategies")
            if strategies:
                wm.addContent("<b><a href='/play' style='float:right;'>PLAY!</a></b>")

            wm.addContent("<br/>Status: ")
            wm.addContent('<span class="status_accepting">ACCEPTING STRATEGIES</span><br/>')
            wm.addContent('<i>Use your citadela client to upload your strategy.</i>')

            wm.addContent("<br/><br/><h3>Registered strategies</h3>")
            thead = ["name", "version", "author"]
            rows = []
            if strategies:
                for skey in strategies:
                    strategy = strategies[skey]
                    rows.append([strategy.name, strategy.version, strategy.author])
                wm.addTable(rows, thead, "width='99%'")
            else:
                wm.addContent("<b>No strategy.</b>")

        if server_status == 2:
            wm.addContent("<br/>Status: ")
            wm.addContent('<span class="status_war">WAR</span><br/>')
            wm.addContent('<i>Please wait until war is ended.</i>')

        if server_status == 3:
            wm.addContent("<br/>Status: ")
            wm.addContent('<span class="status_results">WAR IS OVER</span> (see <a href="/results">results</a>)<br/>')
        self.flushPage(wm)

    def page404(self, path):
        wm = WebMaker("main.tpl")
        wm.title = "Page not found"
        wm.addContent("Requested URL <b>%s</b> not found." % path)
        self.flushPage(wm)
