import time
import thread
import BaseHTTPServer

TPL_VERSION = "0.6"


class WebMaker:
    def __init__(self, template):
        self.template = template

        self.content = []
        self.title = "No title"
        self.version = TPL_VERSION

    def useTemplate(self, vars):
        return open(self.template, 'r').read() % vars

    def addContent(self, data):
        self.content.append(data)

    def addMenuItem(self, title, link):
        self.menu.append('<a href="%s">%s</a>' % (link, title))

    def addTable(self, table, thead=None, tattrs=""):
        self.content.append('<table %s>' % tattrs)
        if thead:
            self.content.append('<tr>')
            for cell in thead:
                self.content.append('<th>%s</th>' % cell)
            self.content.append('</tr>')

        for row in table:
            self.content.append('<tr>')
            for cell in row:
                self.content.append('<td>%s</td>' % cell)
            self.content.append('</tr>')
        self.content.append('</table>')

    def createPage(self):
        content = ''.join(self.content)
        tgen = time.strftime("%d.%m.%Y %H:%M:%S")
        result = self.useTemplate({
            "title": self.title,
            "content": content,
            "version": self.version,
            "timegen": tgen
            })
        return result


class RequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.dispatcher(self.path)

    def flushPage(self, wm):
        self.wfile.write(wm.createPage())

    def dispatcher(self, path):
        pass


class WebServ:
    def __init__(self, rh=RequestHandler, port=80):
        self.running = True
        self.address = ('', port)
        self.handler = rh
        self.server = BaseHTTPServer.HTTPServer(self.address, self.handler)

    def start(self):
        thread.start_new_thread(self.serve_forever, (0,))

    def serve_forever(self, nt=1):
        self.running = True
        while self.running:
            self.handle_request()

    def handle_request(self):
        self.server.handle_request()

    def stop(self):
        self.running = False
