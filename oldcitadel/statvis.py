
def printGameResults(results):
    if results.winner == 0:
        print "REMIZA!"
    else:
        print "VITEZ: %s!" % results.winner

    print "Cas: %.2f s" % results.stats_time

    print "Statistiky:"
    print "  Pocet her: %s" % len(results.stats_games)
    print "  Pocet vitezstvi 1: %s" % results.wins1
    print "  Pocet vitezstvi 2: %s" % results.wins2
    remiza = len(results.stats_games) - (results.wins1 + results.wins2)
    print "  Pocet remiz: %s" % remiza

    remains1wins = []
    remains2wins = []
    remains1lost = []
    remains2lost = []
    length1 = []
    length2 = []
    length_avg = []
    # pocet bitev zemrelych na limit
    battles_limit = 0

    for game in results.stats_games:
        if game[1] == -1:
            battles_limit += 1
        else:
            length_avg.append(game[1])

        if game[0] == 3:
            remains1wins.append(game[2])
            remains2lost.append(game[3])
            length1.append(game[1])
        elif game[0] == -3:
            remains2wins.append(game[3])
            remains1lost.append(game[2])
            length2.append(game[1])

    print "  Pocet her ukoncenych limitem: %s" % battles_limit

    print "  Nejmensi pocet bitev na hru: %s" % min(length_avg)
    print "  Nejvyssi pocet bitev na hru: %s" % max(length_avg)
    n = sum(length_avg) / float(len(length_avg))
    print "  Prumerny pocet bitev na hru: %.2f" % n
    n = sum(length1) / float(len(length1))
    print "  Prumerny pocet bitev na vyhru 1: %.2f" % n
    n = sum(length2) / float(len(length2))
    print "  Prumerny pocet bitev na vyhru 2: %.2f" % n
    n = sum(remains1wins) / float(len(remains1wins))
    print "  Prumer zbylych jednotek ve vyhranych hrach 1: %.2f" % n
    n = sum(remains1lost) / float(len(remains1lost))
    print "  Prumer zbylych jednotek v prohranych hrach 1: %.2f" % n
    n = sum(remains2wins) / float(len(remains2wins))
    print "  Prumer zbylych jednotek ve vyhranych hrach 2: %.2f" % n
    n = sum(remains2lost) / float(len(remains2lost))
    print "  Prumer zbylych jednotek v prohranych hrach 2: %.2f" % n

