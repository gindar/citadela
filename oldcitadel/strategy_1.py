import random
from helpers import Struct


class BasicStrategy:
    def __init__(self, army_size=50):
        self.army_size = army_size

        # remaining soliders
        self.remains = self.army_size

        # position from my citadel to enemy
        # MY -2 -1 0 1 2 ENEMY
        # position == 3 => WIN
        # position == -3 => LOST
        self.position = 0

    def _getSoliders(self):
        v = self.getSoliders()
        v = min(max(v, 1), self.remains)
        self.remains -= v
        return v

    def getSoliders(self):
        return 1

    def warStart(self):
        self.remains = self.army_size
        self.position = 0

    def warEnd(self, result):
        # result:
        #   0 draw
        #   1 win
        #  -1 lost
        pass

    def battleEnd(self, result):
        # result:
        #   0 draw
        #   1 win
        #  -1 lost
        self.position += result


class Strategy1(BasicStrategy):
    def __init__(self, army_size=50):
        BasicStrategy.__init__(self, army_size)

    def getSoliders(self):
        if self.position <= 2:
            return 1
        return random.randint(1, self.remains / 3)


class Strategy2(BasicStrategy):
    def __init__(self, army_size=50):
        BasicStrategy.__init__(self, army_size)

    def getSoliders(self):
        return random.randint(1, self.remains)


class StrategyPlayer:
    def __init__(self, army_size=50):
        self.army_size = army_size
        self.num_of_games = 1000

    def game(self, s1, s2):
        s1wins = 0
        s2wins = 0
        s1inst = s1(self.army_size)
        s2inst = s2(self.army_size)

        for n in xrange(self.num_of_games):
            s1result = 0
            s2result = 0
            s1inst.warStart()
            s2inst.warStart()
            result = self.war(s1inst, s2inst)
            if result == 1:
                s1wins += 1
                s1result = 1
                s2result = -1
            elif result == 2:
                s2wins += 1
                s2result = 1
                s1result = -1
            s1inst.warEnd(s1result)
            s2inst.warEnd(s2result)

        return (s1wins, s2wins)

    def war(self, s1inst, s2inst):
        # position: S1 -2 -1 0 1 2 S2
        info = Struct({
            "s1remains": self.army_size,
            "s2remains": self.army_size,
            "position": 0,
            "winner": 0
        })

        limit = 100
        while limit:
            result = self.battle(s1inst, s2inst, info)
            if result == 1:
                info.position -= 1
            elif result == 2:
                info.position += 1

            if info.position == 3:
                # player 1 wins
                info.winner = 1
                break

            if info.position == -3:
                # player 2 wins
                info.winner = 2
                break

            if info.s1remains == 0 or info.s2remains == 0:
                # player/s has no soliders
                break

            limit -= 1

        return info.winner

    def battle(self, s1i, s2i, info):
        s1n = s1i.getSoliders()
        s2n = s2i.getSoliders()

        # clamp values to range(1, remains)
        s1n = min(max(s1n, 1), info.s1remains)
        s2n = min(max(s2n, 1), info.s2remains)

        # lower remains counters
        info.s1remains -= s1n
        info.s2remains -= s2n

        if s1n < s2n:
            # player 2 win
            s1i.battleEnd(-1)
            s2i.battleEnd(1)
            return 2
        elif s1n > s2n:
            # player 1 win
            s1i.battleEnd(1)
            s2i.battleEnd(-1)
            return 1
        else:
            # plichta
            s1i.battleEnd(0)
            s2i.battleEnd(0)
            return 0

sp = StrategyPlayer()
print sp.game(Strategy1, Strategy2)
