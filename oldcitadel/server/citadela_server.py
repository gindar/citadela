import os
import json
import argparse
import time
try:
    from md5 import md5
except:
    from hashlib import md5

import server
import storage
from events import IEvents

argparser = argparse.ArgumentParser(description='Citadela server.')
argparser.add_argument('config_file', metavar='config_file', type=str, help='Config file')
args = argparser.parse_args()

config = {}

if os.path.exists(args.config_file):
    data = json.load(open(args.config_file, "rb"))
    config["host"] = str(data["host"])
    config["port"] = int(data["port"])
    config["passwd"] = str(data["passwd"])


class ServerHandler(IEvents):
    def __init__(self, config):
        self.config = config
        self.server = server.Server(self.config["host"], self.config["port"])
        self.storage = storage.Storage("storage/")
        self.running = True

        self.players = self.storage.getBinary("__players__")
        if self.players == None:
            self.players = {}
        self.actplayers = {}

        self.listen("server.recv", self.recv)
        self.listen("server.closed", self.quit)

    def savePlayers(self):
        self.storage.saveBinary("__players__", self.players)

    def registerPlayer(self, data):
        msgs = []
        if len(data["name"]) < 3:
            msgs.append("Name is shorter than 3 chars.")
        if len(data["login"]) < 3:
            msgs.append("Login is shorter than 3 chars.")
        if not data["login"].isalnum():
            msgs.append("Login is invalid.")
        if len(data["passwd"]) < 3:
            msgs.append("Password is invalid.")

        plobj = {
            "name": data["name"],
            "login": data["login"],
            "passwd": data["passwd"],
            "strategy_version": 0
        }
        if not msgs:
            self.players[data["login"]] = plobj
            return None
        return msgs

    def strategyUploaded(self, login):
        player = self.getPlayer(login)
        player["strategy_version"] += 1
        print "strategy add", self.strategyID(login)

    def strategyID(self, login):
        player = self.getPlayer(login)
        return "strategy_%s_%s" % (login, player["passwd"])

    def strategySave(self, login, data):
        sfile = self.strategyID(login)
        result = self.storage.addFilePart(sfile, data["data"],
            data["part"], data["parts"])
        return result

    def getPlayer(self, login):
        return self.players[login]

    def loginPlayer(self, data, client):
        if not data["login"] in self.players:
            return False
        player = self.players[data["login"]]
        if player["passwd"] != data["passwd"]:
            return False
        client.auth = data["login"]
        return True

    def quit(self):
        self.running = False
        self.savePlayers()

    def recv(self, client, data):
        if data["cmd"] == "kill":
            if self.config["passwd"] != data["passwd"]:
                client.send({"cmd": "message", "data": "Invalid admin password"})
            else:
                client.send({"cmd": "message", "data": "Successfully killed"})
                self.event("server.quit")

        if data["cmd"] == "register":
            regresult = self.registerPlayer(data)
            if regresult:
                client.send({"cmd": "message",
                    "data": "Registration failed (%s)" % ', '.join(regresult), "attr": 0})
            else:
                self.loginPlayer(data, client)
                client.send({"cmd": "message",
                    "data": "Registration successful. Welcome to CITADELA.", "attr": 1})

        if data["cmd"] == "login":
            if self.loginPlayer(data, client):
                client.send({"cmd": "message", "data": "Login successful", "attr": 1})
            else:
                client.send({"cmd": "message", "data": "Login failed", "attr": 1})

        if client.auth:
            if data["cmd"] == "file":
                result = self.strategySave(client.auth, data)
                if result:
                    client.send({"cmd": "message", "data": "File upload success."})
                    self.strategyUploaded(client.auth)
                else:
                    client.send({"cmd": "fileok"})

    def loop(self):
        while self.running:
            time.sleep(1)
        print "shutdown"

srv_handler = ServerHandler(config)
srv_handler.loop()
