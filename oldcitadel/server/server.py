import socket
import bstruct
import thread
import time
from events import IEvents


class ServerClient(IEvents):
    def __init__(self, client_socket, addr, uid):
        self.sock = client_socket
        self.addr = addr
        self.uid = uid
        self.active = True
        self.auth = False
        self.disconnected = []
        thread.start_new_thread(self.loop, (0, ))

    def loop(self, e=None):
        while self.active:
            data = self.sock.recv(4096)
            if len(data) == 0:
                self.disconnected += 1
                if self.disconnected > 10:
                    self.active = False
            else:
                self.disconnected = 0
            if data:
                data = bstruct.decode(data)
                self.event("server.recv", (self, data))
        print "client disconnected %s" % str(self.addr)
        self.event("server.clientQuit", (self.uid,))

    def send(self, data):
        data = bstruct.encode(data)
        self.sock.sendall(data)


class Server(IEvents):
    def __init__(self, host, port):
        self.host = host
        self.port = port

        self.accepting = True

        self.clients = {}
        self.client_id = 1000

        self.rl = 1

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((self.host, self.port))
        self.sock.listen(1)

        self.listen("server.quit", self.quit)
        self.listen("server.clientQuit", self._clientQuit)

        print "server started %s:%s" % (host, port)
        thread.start_new_thread(self._acceptClients, (0,))

    def quit(self):
        self.accepting = False
        for cid in self.clients:
            self.clients[cid].active = False
        self._quit()

    def _quit(self):
        if self.rl <= 0:
            self.sock.close()
            self.event("server.closed")
            print "server quit"

    def _clientQuit(self, cid):
        if cid in self.clients:
            del self.clients[cid]
            self.rl -= 1
        self._quit()

    def _acceptClients(self, e=None):
        while self.accepting:
            self.client_id += 1
            client_socket, addr = self.sock.accept()
            sc = ServerClient(client_socket, addr, self.client_id)
            self.clients[self.client_id] = sc
            self.rl += 1
            print "client connected %s" % str(addr)
            self.event("server.clientConnected", self.client_id)
            time.sleep(0.3)
        self.rl -= 1
        self._quit()
