import os
import json
import argparse
try:
    from md5 import md5
except:
    from hashlib import md5
import client


def saveConfig(path, data):
    data = json.dumps(data)
    fp = open(path, "wb")
    fp.write(data)
    fp.close()
    print "Configuration saved %s" % path


def getPass():
    passwd = raw_input("Password:")
    return md5(passwd).hexdigest()


def waitForMessage(cli):
    data = cli.recv()
    if "cmd" in data and data["cmd"] == "message":
        print "Server: %s" % data["data"]
        return data.get("attr", None)
    return None

argparser = argparse.ArgumentParser(description='Server remote.')
argparser.add_argument('command', metavar='command', type=str, help='command (configure|register|upload|results)')
argparser.add_argument('--config_file', default=None, help='config file')
args = argparser.parse_args()

if not args.config_file:
    config_file = os.path.expanduser("~/.citadela")
else:
    config_file = args.config_file

config = {}
if os.path.exists(config_file) and args.command != "configure":
    data = json.load(open(config_file, "rb"))
    config["login"] = str(data.get("login", ""))
    config["passwd"] = str(data.get("passwd", ""))
    config["name"] = str(data.get("name", ""))

    config["host"] = str(data["host"])
    config["port"] = int(data["port"])
    print "configuration %s" % config_file
else:
    print "Configuration"
    config["host"] = raw_input("Host (localhost):")
    if config["host"] == "":
        config["host"] = "localhost"
    config["port"] = raw_input("Port (8207):")
    if config["port"] == "":
        config["port"] = 8207
    else:
        config["port"] = int(config["port"])
    saveConfig(config_file, config)

cli = client.Client()
cli.connect(config["host"], config["port"])

if args.command == "register":
    config["login"] = raw_input("Login:")
    name = raw_input("Player name:")
    config["passwd"] = getPass()

    cli.send({
        "cmd": "register",
        "login": config["login"],
        "name": name,
        "passwd": config["passwd"]
        })
    r = waitForMessage(cli)
    if r == 1:
        saveConfig(config_file, config)

if args.command == "login":
    cli.send({
        "cmd": "register",
        "login": config["login"],
        "name": name,
        "passwd": config["passwd"]
        })
