

class IEvents:
    __listeners = {}

    def listen(self, event, callback):
        if not event in self.__listeners:
            self.__listeners[event] = []
        self.__listeners[event].append(callback)

    def event(self, event, args=None):
        if not event in self.__listeners:
            return False

        for listener in self.__listeners[event]:
            if args == None:
                listener()
            else:
                listener(*args)
