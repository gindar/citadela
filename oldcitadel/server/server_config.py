import os
import json
import argparse

try:
    from md5 import md5
except:
    from hashlib import md5

argparser = argparse.ArgumentParser(description='Create/edit server config.')
argparser.add_argument('config_file', metavar='config_file', type=str, help='Config file')
args = argparser.parse_args()
config_file = str(args.config_file)

host = raw_input("Host:")
port = int(raw_input("Port:"))
passwd = raw_input("Enter password:")
passwd = md5(passwd).hexdigest()
print "host: '%s'" % host
print "port: '%s'" % port
print "passwd: '%s'" % passwd

data = {
    "host": host,
    "port": port,
    "passwd": passwd
}
data = json.dumps(data)

fp = open(config_file, "wb")
fp.write(data)
fp.close()
print "Config saved to %s." % config_file
