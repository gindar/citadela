import time
from helpers import Struct

class CitadelaGame:
    def __init__(self):
        self.strategy1 = None
        self.strategy2 = None

        # options
        self.army_size = 50
        self.games_count = 1000
        self.collect_stats = False

        # private
        self.max_game_battles = 0
        self.s1_wins = 0
        self.s2_wins = 0

        self.s1_remains = 0
        self.s2_remains = 0
        self.game_battles = 0
        # P1 -2 -1 0 1 2 P2
        self.position = 0

        self.stats_time = 0
        self.stats_games = []

    def getResults(self):
        winner = 0
        if self.s1_wins > self.s2_wins:
            winner = 1
        elif self.s2_wins > self.s1_wins:
            winner = 2

        results = Struct({
            "winner": winner,
            "wins1": self.s1_wins,
            "wins2": self.s2_wins,
            "stats_time": self.stats_time,
            "stats_games": self.stats_games
        })
        return results

    def play(self, strategy1, strategy2):
        self.strategy1 = strategy1
        self.strategy2 = strategy2

        # reset
        self.max_game_battles = self.army_size + 1
        self.stats_time = 0
        self.stats_games = []
        self.s1_wins = 0
        self.s2_wins = 0

        t = time.time()
        # kill them all!
        for n in xrange(self.games_count):
            self._playGame()
        self.stats_time = time.time() - t

    def _playGame(self):
        self.s1_remains = self.army_size
        self.s2_remains = self.army_size
        self.game_battles = 0
        self.position = 0

        while True:
            # num. of soliders in battle
            s1n = 0
            s2n = 0

            # strategy1 soliders in battle
            if self.s1_remains <= 1:
                s1n = self.s1_remains
            else:
                distance = self.position + 3
                s1n = self.strategy1(self.s1_remains, self.army_size, distance)
                s1n = max(min(int(s1n), self.s1_remains), 0)

            # strategy2 soliders in battle
            if self.s2_remains <= 1:
                s2n = self.s2_remains
            else:
                distance = 3 - self.position
                s2n = self.strategy2(self.s2_remains, self.army_size, distance)
                s2n = max(min(int(s2n), self.s2_remains), 0)

            self.s1_remains -= s1n
            self.s2_remains -= s2n

            if s1n > s2n:
                # s1 wins the battle
                self.position += 1
            elif s1n < s2n:
                # s2 wins the battle
                self.position -= 1

            self.game_battles += 1

            # everbody is dead
            if self.s1_remains <= 0 and self.s2_remains <= 0:
                break

            # defeated
            if abs(self.position) == 3:
                break

            # max battles limit reached
            if self.game_battles == self.max_game_battles:
                self.game_battles = -1
                break
        # end while

        if self.position == 3:
            self.s1_wins += 1

        if self.position == -3:
            self.s2_wins += 1

        if self.collect_stats:
            self.stats_games.append((self.position, self.game_battles,
                self.s1_remains, self.s2_remains))
