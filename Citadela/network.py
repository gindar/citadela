'''
    Client(host, port, ipv6=False)
        .start()
        .quit()
        .recv()
        .send(cmd[, data])

    Server(host, port, ipv6=False)
        .start()
        .quit()
        .recv()
        .send(cmd[, data[, addr]])
        .sendBroadcast(cmd[, data])
'''


import socket
import thread
import bstruct
import time

ERROR_BIND = 0x01
ERROR_RECV = 0x03
ERROR_SEND = 0x04

DEBUG_NONE = 0x00
DEBUG_MIN = 0x01
DEBUG_ALL = 0x02

TEST_DEBUG_DELAY = 0


__all__ = [
    "Server", "Client", "BaseUDP",
    "DEBUG_NONE", "DEBUG_MIN", "DEBUG_ALL",
    "ERROR_BIND", "ERROR_RECV", "ERROR_SEND"
]


'''
    Basic UDP communication class.
'''


class BaseUDP:
    def __init__(self, host, port, ipv6=False):
        if ipv6:
            self.addr_family = socket.AF_INET6
        else:
            self.addr_family = socket.AF_INET

        self.server_addr = (host, port)
        self.default_addr = self.server_addr

        self.running = False
        self.recv_queue = []

        # function called after error
        self.errorCallback = None

        # default target addr
        self.default_addr = None

        self.max_length = 8192

        self.debug = DEBUG_ALL
        self.debug_msgs = []

        # request counter
        self.counter = 1

        self.socket = socket.socket(self.addr_family, socket.SOCK_DGRAM)

    def dbgmsg(self, msg, t=DEBUG_MIN):
        if self.debug >= t:
            print "DEBUG %s" % msg
            self.debug_msgs.append(msg)

    def _errorCallback(self, status_id, data):
        self.dbgmsg("ntw error(%s): %s" % (status_id, data), DEBUG_MIN)
        if self.errorCallback:
            self.errorCallback(status_id, data)

    def send(self, cmd, data=None, addr=None):
        ''' Add request to queue '''
        if not addr:
            addr = self.default_addr

        if TEST_DEBUG_DELAY:
            thread.start_new_thread(self._delayed_send, (cmd, data, addr))
        else:
            self._send(cmd, data, addr)

    def recv(self):
        ''' Return recieved data. '''
        result = self.recv_queue
        self.recv_queue = []
        return result

    def _delayed_send(self, cmd, data, addr):
        time.sleep(TEST_DEBUG_DELAY)
        self._send(cmd, data, addr)

    def _send(self, cmd, data, addr):
        ''' Encoding and sending process. '''
        try:
            t = int(time.time() * 1000)
            _data = bstruct.encode((cmd, data, self.counter, t))
            if self.max_length < len(_data):
                raise Exception("Data length > %s." % self.max_length)
            self.socket.sendto(_data, addr)
            self.dbgmsg("SEND cmd:%s cnt:%s len:%sb (%s:%s)" % (
                cmd, self.counter, len(_data), addr[0], addr[1]
            ), DEBUG_ALL)
            self.counter += 1
        except Exception, e:
            self._errorCallback(ERROR_SEND, e)

    def _loop(self):
        ''' Start send/recv loops in different threads. '''
        thread.start_new_thread(self._loop_recv, (0,))

    def _loop_recv(self, a=0):
        ''' Recieving loop.'''
        while self.running:
            try:
                t = int(time.time() * 1000)
                _data, addr = self.socket.recvfrom(self.max_length)
                cmd, data, cnt, _t = bstruct.decode(_data)
                delay = t - _t
                self.dbgmsg("RECV cmd:%s cnt:%s len:%sb (%s:%s)" % (
                    cmd, self.counter, len(_data), addr[0], addr[1]
                ), DEBUG_ALL)
                self.recv_queue.append((cmd, data, cnt, addr, delay))
            except Exception, e:
                # recieving failed
                self._errorCallback(ERROR_RECV, e)

    def quit(self):
        ''' Stop loops and close socket.'''
        self.running = False
        if self.socket:
            self.socket.close()
            self.socket = None


''' Basic client sender/receiver '''


class Client(BaseUDP):
    def __init__(self, host, port, ipv6=False):
        ''' Init BaseUDP and setup socket '''
        BaseUDP.__init__(self, host, port, ipv6)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.default_addr = self.server_addr

    def start(self):
        ''' Start receiving loop '''
        self.running = True
        self._loop()


''' Basic server sender/receiver '''


class Server(BaseUDP):
    def __init__(self, host, port, ipv6=False):
        BaseUDP.__init__(self, host, port, ipv6)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        #self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    def start(self):
        ''' Bind and start receive loop '''
        self.running = True

        try:
            self.socket.bind((self.server_addr[0], self.server_addr[1]))
            self.dbgmsg("Server bind %s:%s" % (self.server_addr[0], self.server_addr[1]))
        except Exception, e:
            # IF BINDING IS NOT SUCCESSFUL THEN SEND
            # ERROR SIGNAL and stop recv loop
            self.running = False
            self._errorCallback(ERROR_BIND, e)
        self._loop()

    def sendBroadcast(self, cmd, data=None):
        ''' broadcast command with data '''
        addr = ("<broadcast>", self.server_addr[1])
        self.send(cmd, data, addr)
