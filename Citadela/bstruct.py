### From GAZ
### Josef Vanzura <gindar@zamraky.cz>
from cStringIO import StringIO


def uint2byte(num, length=1):
    out = ""
    while length:
        out += chr(num & 255)
        num = num >> 8
        length += -1
    return out


def byte2uint(string):
    num = 0
    q = 1
    l = len(string)
    while q <= l:
        num = num << 8
        ch = string[-q]
        num = num | ord(ch)
        q += 1
    return num

TYPE_NONE = 0x01
TYPE_BOOL_1 = 0x02
TYPE_BOOL_0 = 0x03

TYPE_INT8 = 0x10
TYPE_INT16 = 0x11
TYPE_INT32 = 0x12
TYPE_INT64 = 0x13
TYPE_INT256 = 0x14
TYPE_INT_NEG = 0x20

TYPE_FLOAT = 0x50
TYPE_FLOATB = 0x51
TYPE_STRING = 0x60
TYPE_UNICODE = 0x61
TYPE_STRINGL = 0x62
TYPE_UNICODEL = 0x63

TYPE_LIST = 0xa0
TYPE_TUPLE = 0xa1
TYPE_DICT = 0xa2

cTYPE_FLOAT = chr(TYPE_FLOAT)
cTYPE_FLOATB = chr(TYPE_FLOATB)
cTYPE_STRING = chr(TYPE_STRING)
cTYPE_STRINGL = chr(TYPE_STRINGL)
cTYPE_UNICODE = chr(TYPE_UNICODE)
cTYPE_UNICODEL = chr(TYPE_UNICODEL)
cTYPE_NONE = chr(TYPE_NONE)
cTYPE_BOOL_1 = chr(TYPE_BOOL_1)
cTYPE_BOOL_0 = chr(TYPE_BOOL_0)
cTYPE_LIST = chr(TYPE_LIST)
cTYPE_TUPLE = chr(TYPE_TUPLE)
cTYPE_DICT = chr(TYPE_DICT)
cTYPE_INT8 = chr(TYPE_INT8)
cTYPE_INT16 = chr(TYPE_INT16)
cTYPE_INT32 = chr(TYPE_INT32)
cTYPE_INT64 = chr(TYPE_INT64)
cTYPE_INT256 = chr(TYPE_INT256)
cTYPE_INT_NEG = chr(TYPE_INT_NEG)


def encode(v, a=0, b=0):
    return _encode(v)


def _encode(v):
    if isinstance(v, str):
        return encode_str(v)
    elif isinstance(v, unicode):
        return encode_unicode(v)
    elif isinstance(v, bool):
        return encode_bool(v)
    elif v is None:
        return cTYPE_NONE
    elif isinstance(v, int) or isinstance(v, long):
        return encode_int(v)
    elif isinstance(v, float):
        return encode_float(v)

    elif isinstance(v, tuple):
        return encode_tuple(v)
    elif isinstance(v, list):
        return encode_list(v)
    elif isinstance(v, dict):
        return encode_dict(v)
    else:
        raise TypeError("Unknown data type %s" % type(v))


def encode_bool(_value):
    if _value:
        return cTYPE_BOOL_1
    else:
        return cTYPE_BOOL_0


def encode_float(_value):
    if _value > 0xFF:
        _v = int(_value * 0xFFFFFF)
        v = (cTYPE_FLOATB, encode_int(_v))
    else:
        _v = int(_value * 0xFFFFFFFFFFFFF)
        v = (cTYPE_FLOAT, encode_int(_v))
    return "%s%s" % v


def encode_str(_value):
    if len(_value) < 255:
        lenenc = chr(len(_value))
        return "%s%s%s" % (cTYPE_STRING, lenenc, _value)
    else:
        lenenc = encode_int(len(_value))
        return "%s%s%s" % (cTYPE_STRINGL, lenenc, _value)


def encode_unicode(_value):
    _value = _value.encode("utf-8")
    if len(_value) < 255:
        lenenc = chr(len(_value))
        return "%s%s%s" % (cTYPE_UNICODE, lenenc, _value)
    else:
        lenenc = encode_int(len(_value))
        return "%s%s%s" % (cTYPE_UNICODEL, lenenc, _value)


def encode_int(_value):
    value = abs(_value)
    if value < 0xFF:
        v = (cTYPE_INT8, chr(value))
    elif value < 0xFFFF:
        v = (cTYPE_INT16, uint2byte(value, 2))
    elif value < 0xFFFFFFFF:
        v = (cTYPE_INT32, uint2byte(value, 4))
    elif value < 0xFFFFFFFFFFFFFFFF:
        v = (cTYPE_INT64, uint2byte(value, 8))
    else:
        v = (cTYPE_INT256, uint2byte(value, 32))
    if _value < 0:
        return "%s%s%s" % (cTYPE_INT_NEG, v[0], v[1])
    else:
        return "%s%s" % v


def encode_iter(_value):
    count = 0
    nlist = []
    for item in _value:
        nlist.append(_encode(item))
        count += 1
    count = encode_int(count)
    nlist = ''.join(nlist)
    return "%s%s" % (count, nlist)


def encode_list(_value):
    return "%s%s" % (cTYPE_LIST, encode_iter(_value))


def encode_tuple(_value):
    return "%s%s" % (cTYPE_TUPLE, encode_iter(_value))


def encode_dict(_value):
    keys = encode_iter(_value.keys())
    values = encode_iter(_value.values())
    return "%s%s%s" % (cTYPE_DICT, keys, values)

########################### DECODE


def decode(v):
    if isinstance(v, str):
        v = StringIO(v)
    return _decode(v)


def _decode(fp):
    t = ord(fp.read(1))
    if t == TYPE_STRING:
        return decode_str(fp)
    elif t == TYPE_UNICODE:
        return decode_unicode(fp)
    elif TYPE_INT8 <= t <= TYPE_INT256 or t == TYPE_INT_NEG:
        return decode_int(fp, t)
    elif t == TYPE_FLOAT:
        return decode_float(fp)
    elif t == TYPE_FLOATB:
        return decode_floatb(fp)
    elif t == TYPE_BOOL_0:
        return False
    elif t == TYPE_BOOL_1:
        return True
    elif t == TYPE_NONE:
        return None
    elif t == TYPE_LIST:
        return decode_list(fp)
    elif t == TYPE_TUPLE:
        return decode_tuple(fp)
    elif t == TYPE_DICT:
        return decode_dict(fp)
    elif t == TYPE_STRINGL:
        return decode_strL(fp)
    elif t == TYPE_UNICODEL:
        return decode_unicodeL(fp)


def decode_float(fp):
    return float(_decode(fp)) / 0xFFFFFFFFFFFFF


def decode_floatb(fp):
    return float(_decode(fp)) / 0xFFFFFF


def decode_str(fp):
    lendec = ord(fp.read(1))
    return fp.read(lendec)


def decode_unicode(fp):
    lendec = ord(fp.read(1))
    return unicode(fp.read(lendec), "utf-8")


def decode_strL(fp):
    lendec = decode_int(fp)
    return fp.read(lendec)


def decode_unicodeL(fp):
    lendec = decode_int(fp)
    return unicode(fp.read(lendec), "utf-8")


def decode_int(fp, t=None):
    if not t:
        t = ord(fp.read(1))
    if t == TYPE_INT_NEG:
        t = ord(fp.read(1))
        return decode_int(fp, t) * -1
    if t == TYPE_INT8:
        v = ord(fp.read(1))
    elif t == TYPE_INT16:
        v = byte2uint(fp.read(2))
    elif t == TYPE_INT32:
        v = byte2uint(fp.read(4))
    elif t == TYPE_INT64:
        v = byte2uint(fp.read(8))
    elif t == TYPE_INT256:
        v = byte2uint(fp.read(32))
    return v


def decode_iter(fp):
    result = []
    count = decode_int(fp)
    for n in xrange(count):
        v = _decode(fp)
        result.append(v)
    return result


def decode_list(fp):
    return decode_iter(fp)


def decode_tuple(fp):
    return tuple(decode_iter(fp))


def decode_dict(fp):
    result = {}
    keys = decode_iter(fp)
    values = decode_iter(fp)
    for i in xrange(len(keys)):
        result[keys[i]] = values[i]
    return result
