import time
import os
from helpers import IEvents


'''
    Simple logger
'''


class CitadelaLogger(IEvents):
    def __init__(self, logfile_path):
        self.debuglevel = "all"
        self.logfile_path = logfile_path
        self.logfile = None

        self.listen("log-error", self.logError)
        self.listen("log-info", self.logInfo)

    def _logToFile(self, t, msg):
        if not self.logfile:
            lname = time.strftime("citadela_%y.%m.%d_%H.log")
            fname = os.path.join(self.logfile_path, lname)
            self.logfile = fname
        ti = time.strftime("%H:%M:%S")
        fp = open(self.logfile, "ab")
        fp.write("%s %s %s\n" % (ti, t, msg))
        fp.close()

    def logError(self, name, obj="", data=""):
        msg = "%s %s %s" % (name, obj, data)
        print "Error: %s" % msg
        self._logToFile("ERROR", msg)

    def logInfo(self, name, obj="", data=""):
        msg = "%s %s %s" % (name, obj, data)
        if self.debuglevel != "onlyerrors":
            print "Info: %s" % msg
            self._logToFile("INFO", msg)
