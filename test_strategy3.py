from Citadela.game import BasicStrategy


class Strategy(BasicStrategy):
    name = "Strategy3"
    author = "gindar"
    version = "1.0"

    def __init__(self, army_size=50):
        BasicStrategy.__init__(self, army_size)

    def getSoldiers(self):
        if self.remains < 3:
            return 1
        if self.position <= 2:
            return 1
        return self.random.randint(1, self.remains / 3)
